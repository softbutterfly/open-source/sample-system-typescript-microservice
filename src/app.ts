// app.ts
import express, { Request, Response } from 'express';
import * as newrelic from 'newrelic';

interface Task {
    id: number;
    description: string;
    completed: boolean;
  }
  
class TaskManager {
  private tasks: Task[] = [];

  addTask(description: string): void {
    const newTask: Task = {
      id: this.tasks.length + 1,
      description,
      completed: false,
    };
    this.tasks.push(newTask);
  }

  markTaskAsCompleted(id: number): void {
    const task = this.tasks.find(task => task.id === id);
    if (task) {
      task.completed = true;
    }
  }
  
  getAllTasks(req: Request, res: Response): void {
    res.json(this.tasks);
  }
}

const app = express();
const port = 3000;

const taskManager = new TaskManager();
taskManager.addTask('Hacer la compra');
taskManager.addTask('Estudiar TypeScript');
taskManager.addTask('Hacer demos');
taskManager.markTaskAsCompleted(1);
app.get('/', (req, res) => taskManager.getAllTasks(req, res));

app.listen(port, () => {
  console.log(`Servidor iniciado en http://localhost:${port}`);
});  