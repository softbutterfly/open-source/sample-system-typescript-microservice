'use strict';
require('dotenv').config();

const appName = process.env.NEW_RELIC_APP_NAME;
const license_key = process.env.NEW_RELIC_LICENSE_KEY;

exports.config = {
  app_name: [appName],
  license_key: license_key,
  logging: {
    level: 'info',
  },
};